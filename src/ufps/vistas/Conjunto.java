package ufps.vistas;

import java.util.*;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author OMAR
 */
public class Conjunto {

    public static <T> void pintarMultiLista(ListaS<ListaS<T>> conjuntos) {
        int contadorSub, contadorCon = 0;
        System.out.println("P(A)={");

        for (ListaS<T> subConjuntos : conjuntos) {
            String conjuntoP = "{";
            contadorSub = 0;
            for (Object object : subConjuntos) {
                conjuntoP += object.toString();
                if (contadorSub++ < subConjuntos.getTamanio() - 1) {
                    conjuntoP += ',';
                }
            }
            conjuntoP += '}';
            if (contadorCon++ < conjuntos.getTamanio() - 1) {
                conjuntoP += ", ";
            }
            System.out.println(conjuntoP);
        }
        System.out.println("}.");
    }

    public static void main(String[] args) {
        ListaS<Integer> lista = new ListaS<>();

        for (int i = 1; i <= 17; i++) {
            lista.insertarAlFinal(i);
        }

        Date fechaInicio = new Date();

        ListaS<ListaS<Integer>> conjuntos = lista.getConjuntoPar();

        Date fechaFinal = new Date();

        Conjunto.pintarMultiLista(conjuntos);
        System.out.println(((fechaFinal.getTime() - fechaInicio.getTime())) + " milisegundos");
        System.out.println(conjuntos.getTamanio() + " conjuntos");
    }

}
