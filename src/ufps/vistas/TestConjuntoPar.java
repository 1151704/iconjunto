package ufps.vistas;

import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author OMAR
 */
public class TestConjuntoPar {
    
    public static void main(String[] args) {
        unEjemploDeMultiLista();
        
        ListaS<Integer> ejemploLista = new ListaS();
        //Creando una lista de prueba:
        ejemploLista.insertarAlFinal(2);
        ejemploLista.insertarAlFinal(4);
        ejemploLista.insertarAlFinal(5);
        //Llamando el método conjuntoPar:
        ListaS<ListaS<Integer>> par = ejemploLista.getConjuntoPar();
        //Imprimir caso de prueba:
        System.out.println("Multilista generada por conjunto par:");
        Conjunto.pintarMultiLista(par);
        
    }
    
    private static void unEjemploDeMultiLista() {

        //Creando la Multilista:
        ListaS<ListaS<Integer>> m = new ListaS();

        //Creando la lista1:
        ListaS<Integer> l1 = new ListaS();
        l1.insertarAlFinal(1);
        l1.insertarAlFinal(2);

        //Creando la lista2:
        ListaS<Integer> l2 = new ListaS();
        l2.insertarAlFinal(5);
        l2.insertarAlFinal(8);
        l2.insertarAlFinal(10);

        //Insertando estas listas en la Multilista
        m.insertarAlFinal(l1);
        m.insertarAlFinal(l2);
        
        System.out.println("Ejemplo de multilista:");
        Conjunto.pintarMultiLista(m);
    }
    
}
